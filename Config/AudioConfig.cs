using homelleon.AudioJect.Data;
using UnityEngine;

namespace homelleon.AudioJect.Config
{
    [CreateAssetMenu(fileName = "AudioConfig", menuName = "Settings/AudioConfig")]
    public class AudioConfig : ScriptableObject
    {
        [field: SerializeField] public MixerGroupPair[] Groups { get; private set; }
    }
}