using homelleon.AudioJect.Data;
using UnityEngine;

namespace homelleon.AudioJect.Config
{
    [CreateAssetMenu(fileName = "Tracks", menuName = "Settings/Tracks")]
    public class AudioTracksConfig : ScriptableObject
    {
        [SerializeField] private AudioClipData[] _tracks;
        public AudioClipData[] Tracks => _tracks;
    }
}
