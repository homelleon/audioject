using homelleon.AudioJect.Data;

namespace homelleon.AudioJect
{
    public interface IAudioManager
    {
        void Play(string name, bool doLoop = false);
        void Stop(string name);
        void Stop();
    }
}
