using System;
using UnityEngine;

namespace homelleon.AudioJect.Data
{
    [Serializable]
    public struct AudioClipData
    {
        public string Name;
        public MixerGroupName Type;
        public AudioClip Clip;
    }
}
