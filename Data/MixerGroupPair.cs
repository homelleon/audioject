using System;
using UnityEngine.Audio;

namespace homelleon.AudioJect.Data
{
    [Serializable]
    public struct MixerGroupPair
    {
        public MixerGroupName Name;
        public AudioMixerGroup Group;
    }
}
