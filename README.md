# AudioJect
AudioManager package for memory-safe Audio usage.
Supposed to be used with Zenject.
- - -
Place AudioSourcePool object onto GameObject in your Scene or Inject it on new GameObject.  
Inject classes like in this example:

```
[CreateAssetMenu(fileName = "ProjectInstaller", menuName = "Installers/ProjectInstaller")]
public class ProjectInstaller : ScriptableObjectInstaller<ProjectInstaller>
{
    [SerializeField] private AudioConfig _audioConfig;
    [SerializeField] private AudioTracksConfig _tracks;
    public override void InstallBindings()
    {
        Container.BindInstances(_audioConfig, _tracks);
        Container.BindFactory<Transform, AudioSource, AudioSourceFactoryPlaceholder>().FromFactory<AudioSourceFactory>();
        Container.BindInterfacesAndSelfTo<AudioSourcePool>().FromNewComponentOnNewGameObject().AsSingle();
        Container.BindInterfacesAndSelfTo<AudioManager>().AsSingle();
    }
}
```
Create in your project folder config files threw context menu "Settings"->"AudioConfig" and "Settings"->"Tracks".  
Fill in and place install it into your ProjectInstaller ScriptableObject.  
Read about Zenject to find out how to install SceneContext into your scenes.