using UnityEngine;
using Zenject;

namespace homelleon.AudioJect.Implementation
{
    public class AudioSourceFactory : IFactory<Transform, AudioSource>
    {
        private DiContainer _container;
        public AudioSourceFactory(DiContainer container)
        {
            _container = container;
        }

        public AudioSource Create(Transform parent)
        {
            var source = _container.InstantiateComponentOnNewGameObject<AudioSource>();
            source.transform.parent = parent;
            return source;
        }
    }
    public class AudioSourceFactoryPlaceholder : PlaceholderFactory<Transform, AudioSource> { }
}
