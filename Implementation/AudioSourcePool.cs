using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace homelleon.AudioJect.Implementation
{
    public class AudioSourcePool : MonoBehaviour, IAudioSourcePool
    {
        [Inject] private AudioSourceFactoryPlaceholder _factory;
        private Queue<AudioSource> _pool = new Queue<AudioSource>();

        public void Abandone(AudioSource source)
        {
            if (source != null)
                source.gameObject.SetActive(false);
            _pool.Enqueue(source);
        }

        public AudioSource Get() => 
            _pool.Count > 0 ? Pull() : Allocate();

        private AudioSource Pull()
        {
            var source = _pool.Dequeue();
            source.transform.parent = transform;
            return source;
        }

        private AudioSource Allocate() =>
            _factory.Create(transform);
    }
}
