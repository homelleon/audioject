using homelleon.AudioJect.Config;
using homelleon.AudioJect.Data;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

namespace homelleon.AudioJect.Implementation
{
    public class AudioManager : IAudioManager
    {
        private AudioTracksConfig _tracksConfig;
        private AudioConfig _audioConfig;
        private IAudioSourcePool _pool;
        private Dictionary<string, AudioClipData> _tracks = new Dictionary<string, AudioClipData>();
        private Dictionary<string, AudioSource> _sources = new Dictionary<string, AudioSource>();
        private Dictionary<MixerGroupName, AudioMixerGroup> _groups = new Dictionary<MixerGroupName, AudioMixerGroup>();
        private List<string> _playing = new List<string>();
        private string _playingMusic;

        public AudioManager(AudioTracksConfig tracksConfig, AudioConfig audioConfig, IAudioSourcePool pool)
        {
            _tracksConfig = tracksConfig;
            _audioConfig = audioConfig;
            _pool = pool;

            var tracksArray = _tracksConfig.Tracks;
            for (var i = 0; i < tracksArray.Length; i++)
            {
                var trackPair = _tracksConfig.Tracks[i];
                _tracks.Add(trackPair.Name, trackPair);
            }

            var groups = _audioConfig.Groups;
            for (var i = 0; i < groups.Length; i++)
            {
                var groupPair = groups[i];
                _groups.Add(groupPair.Name, groupPair.Group);
            }
        }

        public void Play(string name, bool doLoop = false)
        {
            if (_tracks[name].Type == MixerGroupName.Music)
            {
                if (_playingMusic != null)
                    Stop(_playingMusic);

                _playingMusic = name;
            }

            if (!_sources.TryGetValue(name, out AudioSource source))
                source = CreateSource(name);

            source.loop = doLoop;

            if (!_playing.Contains(name))
                _playing.Add(name);

            source.gameObject.SetActive(true);
            source.Play();
        }

        private AudioSource CreateSource(string name)
        {
            var source = _pool.Get();
            var track = _tracks[name];
            source.clip = track.Clip;
            source.outputAudioMixerGroup = _groups[track.Type];
            _sources.Add(name, source);
            return source;
        }

        public void Stop(string name = null)
        {
            if (string.IsNullOrEmpty(name))
                Stop();
            else
            {
                StopInternal(name);
                _playing.Remove(name);
                if (_playingMusic == name)
                    _playingMusic = null;
            }
        }

        public void Stop()
        {
            _playing.ForEach(StopInternal);
            _playing.Clear();
            _playingMusic = null;
        }

        private void StopInternal(string name)
        {
            var source = _sources[name];
            if (source != null)
                source.Stop();
            _sources.Remove(name);
            _pool.Abandone(source);
        }
    }
}
