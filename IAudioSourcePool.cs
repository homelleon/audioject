using UnityEngine;

namespace homelleon.AudioJect
{
    public interface IAudioSourcePool
    {
        AudioSource Get();
        void Abandone(AudioSource source);
    }
}
